from rest_framework.test import APILiveServerTestCase
from rest_framework import status
import requests
import json

from django.contrib.auth import get_user_model
User = get_user_model()


class NewCustomerTest(APILiveServerTestCase):
    def create_user(self, username):
        user = User.objects.create(username=username)
        user.set_password("nothing")
        user.save()
        return user

    def decode_data(self, response):
        return json.loads(response.content.decode())

    def test_simple_key_value_creation_and_retrieval(self):
        # A customer finds our API and begins to use it
        # They request an account from us and begin to use it
        self.create_user("bob")
        bob_auth = ("bob", "nothing")

        # They go to see if there are any keys for them.
        resp = requests.get(self.live_server_url + "/api/get/", auth=bob_auth)

        # There aren't
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(self.decode_data(resp), [])

        # They construct a key-value pair they wish to store
        pair1 = {"key": "foo", "value": "bar"}

        # They send it to our site
        resp = requests.post(self.live_server_url + "/api/store", json=pair1,
                             auth=bob_auth)

        # They see the pair was created.
        self.assertEqual(resp.status_code, status.HTTP_201_CREATED,
                         msg="First save failed!")

        # They decide to create another
        pair2 = {"key": "second", "value": "more text"}
        resp = requests.post(self.live_server_url + "/api/store", json=pair2,
                             auth=bob_auth)

        # Once again, the pair was created
        self.assertEqual(resp.status_code, status.HTTP_201_CREATED,
                         msg="Second save failed!")

        # They go to get their first pair and see it's correct
        resp = requests.get(self.live_server_url + "/api/get/foo/",
                            auth=bob_auth)
        data = self.decode_data(resp)
        self.assertEqual(data["key"], "foo", msg="Not correctly retrieved!")
        self.assertEqual(data["value"], "bar")

        # They go to the retieval endpoint and see all of their pairs
        resp = requests.get(self.live_server_url + "/api/get/", auth=bob_auth)
        self.assertJSONEqual(resp.content.decode(), [pair1, pair2])

        # They wish to update their first key-value pair using a PUT request
        pair1 = {"key": "foo", "value": "updated!"}

        # # Client.put requires data to be serialised and content_type set.
        resp = requests.put(self.live_server_url + "/api/store", json=pair1,
                            auth=bob_auth)

        # They see the pair was updated.
        self.assertEqual(resp.status_code, status.HTTP_204_NO_CONTENT,
                         msg="Update failed!")

        # They check that the pair was updated
        resp = requests.get(self.live_server_url + "/api/get/foo/",
                            auth=bob_auth)
        self.assertEqual(self.decode_data(resp)["value"], "updated!")

        # With that, they are satisfied with using the API

    def test_multiple_users(self):
        # We have two customers, Alice and Bob, who both wish to use the API
        # They are each assigned accounts and open their client apps
        self.create_user("alice")
        self.create_user("bob")
        alice_auth = ("alice", "nothing")
        bob_auth = ("bob", "nothing")

        # Alice creates a KV pair
        requests.post(self.live_server_url + "/api/store",
                      json={"key": "secret", "value": "alice"},
                      auth=alice_auth)

        # Bob can't see Alice's KV pair in his list
        resp = requests.get(self.live_server_url + "/api/get/", auth=bob_auth)
        self.assertEqual(self.decode_data(resp), [])

        # He can't retrieve it by key, either.
        resp = requests.get(self.live_server_url + "/api/get/secret/",
                            auth=bob_auth)
        self.assertEqual(resp.status_code, status.HTTP_404_NOT_FOUND)

        # Jealous, Bob makes his own pair, similar to Alice's
        requests.post(self.live_server_url + "/api/store",
                      data={"key": "secret", "value": "bob"},
                      auth=bob_auth)

        # He can retrieve this by key.
        resp = requests.get(self.live_server_url + "/api/get/secret/",
                            auth=bob_auth)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(self.decode_data(resp)["value"], "bob")

        # Both Alice and Bob are happy and move on to something else.
