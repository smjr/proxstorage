from django.conf.urls import url

from storer import views

urlpatterns = [
    url(r"^get/$", views.get_all_pairs, name="get_all_pairs"),
    url(r"^get/(?P<key>\w{1,20})/$", views.get_pair, name="get_pair"),
    url(r"^store$", views.store_pair, name="store_pair")
]
