from functools import wraps
from rest_framework.response import Response
from rest_framework import status


def login_required_no_redirect(view_func):
    @wraps(view_func)
    def decorator(request, *args, **kwargs):
        if request.user.is_authenticated():
            return view_func(request, *args, **kwargs)
        return Response(status=status.HTTP_403_FORBIDDEN)
    return decorator
