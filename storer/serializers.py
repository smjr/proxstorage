from rest_framework import serializers

from .models import Pair


class PairSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pair
        fields = ("key", "value")


class WritablePairSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source="owner.username")

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def perform_update(self, serializer):
        serializer.save(owner=self.request.user)

    class Meta:
        model = Pair
        fields = ("key", "value", "owner")
