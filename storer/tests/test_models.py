from django.test import TestCase
from django.core.exceptions import ValidationError
from django.contrib.auth import get_user_model

from storer.models import Pair
User = get_user_model()


class PairModelTest(TestCase):
    def create_user(self, username):
        return User.objects.create(username=username)

    def test_pair_cannot_be_saved_with_an_empty_key(self):
        pair = Pair(value="bar", owner=self.create_user("bob"))
        with self.assertRaises(ValidationError):
            pair.full_clean()

    def test_pair_can_be_saved_with_blank_value(self):
        pair = Pair(key="foo", value="bar", owner=self.create_user("bob"))
        pair.save()
        pair.full_clean()  # Should not raise

    def test_pair_cannot_be_saved_with_invalid_key(self):
        pair = Pair(key="x" * 21, owner=self.create_user("bob"))
        with self.assertRaises(ValidationError):
            pair.full_clean()

    def test_pair_cannot_be_saved_with_invalid_value(self):
        pair = Pair(key="foo", value="x" * 101, owner=self.create_user("bob"))
        with self.assertRaises(ValidationError):
            pair.full_clean()

    def test_pair_cannot_be_saved_with_existing_key(self):
        bob = self.create_user("bob")
        Pair.objects.create(key="foo", value="bar", owner=bob)
        pair = Pair(key="foo", value="x", owner=bob)
        with self.assertRaises(ValidationError):
            pair.full_clean()

    def test_string_representation(self):
        pair = Pair(key="foo", value="bar", owner=self.create_user("bob"))
        self.assertEqual(str(pair), "foo -> bar")

    def test_pair_is_related_to_user(self):
        user = self.create_user("bob")
        pair = Pair(key="foo", owner=user)
        pair.save()
        self.assertIn(pair, user.pair_set.all())

    def test_duplicate_pairs_for_multiple_users_can_exist(self):
        alice = self.create_user("alice")
        bob = self.create_user("bob")

        Pair.objects.create(key="foo", value="bar", owner=alice)
        pair = Pair(key="foo", value="bar", owner=bob)
        pair.full_clean()  # Should not raise
