from django.test import TestCase
from django.contrib.auth import get_user_model

from storer.models import Pair
from storer.serializers import PairSerializer, WritablePairSerializer
User = get_user_model()


class PairSerializerTest(TestCase):
    def test_seralizes_valid_pair(self):
        user = User.objects.create(username="bob")
        pair = Pair.objects.create(key="foo", value="bar", owner=user)
        s_pair = PairSerializer(pair)
        self.assertDictEqual(s_pair.data, {"key": "foo", "value": "bar"})


class WritablePairSerializerTest(TestCase):
    def test_seralizes_valid_pair(self):
        user = User.objects.create(username="bob")
        pair = Pair.objects.create(key="foo", value="bar", owner=user)
        s_pair = WritablePairSerializer(pair)
        self.assertDictEqual(s_pair.data, {"key": "foo", "value": "bar",
                                           "owner": "bob"})

    def test_creates_pair_with_valid_data(self):
        user = User.objects.create(username="bob")
        s_pair = WritablePairSerializer(data={
            "key": "foo",
            "value": "bar",
        })
        self.assertTrue(s_pair.is_valid())
        s_pair.save(owner=user)
        self.assertEqual(Pair.objects.count(), 1)

    def test_does_not_create_pair_with_no_user(self):
        s_pair = WritablePairSerializer(data={
            "key": "foo",
            "value": "bar",
        })
        with self.assertRaises(ValueError):
            s_pair.is_valid()
            s_pair.save(owner=None)
        self.assertEqual(Pair.objects.count(), 0)
