from django.test import TestCase, Client
from django.contrib.auth import get_user_model
from rest_framework import status
import json

from storer.models import Pair
User = get_user_model()


def create_user(username):
    return User.objects.create(username=username)


class RetrieveAllViewTest(TestCase):
    def test_initial_state_returns_empty(self):
        self.client.force_login(user=create_user("bob"))
        resp = self.client.get("/api/get/")
        self.assertEqual(resp.data, [])

    def test_returns_all_user_items(self):
        user = create_user("bob")
        self.client.force_login(user=user)
        Pair.objects.create(key="foo", value="bar", owner=user)
        resp = self.client.get("/api/get/")
        self.assertEqual(resp.data, [{"key": "foo", "value": "bar"}])

    def test_does_not_return_other_users_items(self):
        alice = create_user("alice")
        bob = create_user("bob")

        Pair.objects.create(key="foo", value="bar", owner=alice)

        self.client.force_login(user=bob)
        resp = self.client.get("/api/get/")
        self.assertEqual(resp.data, [])

    def test_does_not_allow_unauthenticated_use(self):
        resp = self.client.get("/api/get/")
        self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN)


class RetrieveViewTest(TestCase):
    def setUp(self):
        self.user = create_user("bob")
        self.client.force_login(user=self.user)

    def test_view_returns_specified_pair(self):
        Pair.objects.create(key="foo", value="bar", owner=self.user)
        resp = self.client.get("/api/get/foo/")
        self.assertDictEqual(resp.data, {"key": "foo", "value": "bar"})

    def test_valid_request_returns_200(self):
        Pair.objects.create(key="foo", value="bar", owner=self.user)
        resp = self.client.get("/api/get/foo/")
        self.assertEqual(resp.status_code, status.HTTP_200_OK)

    def test_view_returns_404_if_no_data(self):
        resp = self.client.get("/api/get/foo/")
        self.assertEqual(resp.status_code, status.HTTP_404_NOT_FOUND)

    def test_view_returns_error_if_no_data(self):
        resp = self.client.get("/api/get/foo/")
        self.assertEqual(resp.data["errors"], "Key not found")

    def test_view_returns_404_if_key_too_long(self):
        # Creating this object will violate integrity rules, but it will be
        # created. This is only for testing the URL config.
        Pair.objects.create(key="f" * 21, value="bar", owner=self.user)
        resp = self.client.get("/api/get/fffffffffffffffffffff/")
        self.assertEqual(resp.status_code, status.HTTP_404_NOT_FOUND)

    def test_does_not_allow_unauthenticated_use(self):
        unauthed_client = Client()
        Pair.objects.create(key="foo", value="bar", owner=self.user)
        resp = unauthed_client.get("/api/get/foo/")
        self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN)

    def test_returns_404_if_pair_does_not_belong_to_user(self):
        alice = create_user("alice")
        Pair.objects.create(key="foo", value="bar", owner=alice)
        resp = self.client.get("/api/get/foo/")
        self.assertEqual(resp.status_code, status.HTTP_404_NOT_FOUND)


class StorageViewTest(TestCase):
    def setUp(self):
        self.user = create_user("bob")
        self.client.force_login(user=self.user)

    def test_can_store_a_key_value_pair(self):
        pair = {"key": "foo", "value": "bar"}
        self.client.post("/api/store", data=pair)
        self.assertEqual(Pair.objects.count(), 1)
        new_pair = Pair.objects.first()

        self.assertEqual(new_pair.key, "foo")
        self.assertEqual(new_pair.value, "bar")

    def test_returns_201_on_valid_data(self):
        pair = {"key": "foo", "value": "bar"}
        resp = self.client.post("/api/store", data=pair)
        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)

    def test_cannot_store_invalid_input(self):
        pair = {"key": "", "value": ""}
        self.client.post("/api/store", data=pair)
        self.assertEqual(Pair.objects.count(), 0)

    def test_returns_400_and_error_message_on_invalid_input(self):
        pair = {"key": "", "value": ""}
        resp = self.client.post("/api/store", data=pair)
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("errors", resp.data)

    def test_returns_400_and_error_message_on_POST_with_existing_key(self):
        Pair.objects.create(key="foo", value="", owner=self.user)
        pair = {"key": "foo", "value": "bar"}
        resp = self.client.post("/api/store", data=pair)
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(resp.data["errors"], "Key already exists")

    def test_can_update_a_key_value_pair(self):
        Pair.objects.create(key="foo", value="bar", owner=self.user)
        pair = {"key": "foo", "value": "updated"}

        # Client.put requires data to be serialised and content_type set.
        self.client.put("/api/store", data=json.dumps(pair),
                        content_type="application/json")
        self.assertEqual(Pair.objects.count(), 1)
        new_pair = Pair.objects.first()

        self.assertEqual(new_pair.key, "foo")
        self.assertEqual(new_pair.value, "updated")

    def test_returns_204_on_update(self):
        Pair.objects.create(key="foo", value="bar", owner=self.user)
        pair = {"key": "foo", "value": "updated"}

        # Client.put requires data to be serialised and content_type set.
        resp = self.client.put("/api/store", data=json.dumps(pair),
                               content_type="application/json")
        self.assertEqual(resp.status_code, status.HTTP_204_NO_CONTENT)

    def test_does_not_allow_unauthenticated_use(self):
        unauthed_client = Client()
        resp = unauthed_client.post("/api/store")
        self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN)

    def test_PUT_request_returns_404_if_key_not_found(self):
        pair = {"key": "foo", "value": "bar"}
        resp = self.client.put("/api/store", data=json.dumps(pair),
                               content_type="application/json")
        self.assertEqual(resp.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(resp.data["errors"], "Key not found")

    def test_can_save_duplicate_keys_if_with_different_user(self):
        Pair.objects.create(key="foo", value="bar", owner=create_user("alice"))
        pair = {"key": "foo", "value": "bar"}
        self.client.post("/api/store", data=pair)
        self.assertEqual(Pair.objects.count(), 2)

    def test_can_update_duplicate_keys_if_with_different_user(self):
        Pair.objects.create(key="foo", value="bar", owner=create_user("alice"))
        Pair.objects.create(key="foo", value="bar", owner=self.user)
        pair = {"key": "foo", "value": "updated"}
        self.client.put("/api/store", data=json.dumps(pair),
                        content_type="application/json")
        pair = Pair.objects.get(key="foo", owner=self.user)
        self.assertEqual(pair.value, "updated")
