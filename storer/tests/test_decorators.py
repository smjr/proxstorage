from unittest import TestCase
from unittest.mock import MagicMock
from rest_framework import status

from storer.decorators import login_required_no_redirect


class LoginRequiredDecoratorTest(TestCase):
    def test_decorator_returns_decorated_function_if_user_authenticated(self):
        @login_required_no_redirect
        def dummy_view_func(request):
            return "dummy"

        mock_request = MagicMock()
        mock_request.user.is_authenticated.return_value = True

        self.assertEqual(dummy_view_func(mock_request), "dummy")

    def test_decorator_returns_403_response_if_user_not_authenticated(self):
        @login_required_no_redirect
        def dummy_view_func(request):
            return "dummy"

        mock_request = MagicMock()
        mock_request.user.is_authenticated.return_value = False

        resp = dummy_view_func(mock_request)
        self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN)

    def test_decorator_wraps_func_properly(self):
        @login_required_no_redirect
        def dummy_view_func(request):
            return "dummy"

        self.assertEqual(dummy_view_func.__name__, "dummy_view_func")
