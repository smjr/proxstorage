from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status

from .models import Pair
from .serializers import PairSerializer, WritablePairSerializer
from .decorators import login_required_no_redirect


@api_view(["GET"])
@login_required_no_redirect
def get_all_pairs(request):
    all_pairs = PairSerializer(Pair.objects.filter(owner=request.user),
                               many=True)
    return Response(data=all_pairs.data)


@api_view(["GET"])
@login_required_no_redirect
def get_pair(request, key):
    try:
        # There will never be more than one pair due to unique key and user
        # constraint
        pair_s = PairSerializer(Pair.objects.get(key=key, owner=request.user))
        return Response(data=pair_s.data)
    except Pair.DoesNotExist:
        return Response(data={"errors": "Key not found"},
                        status=status.HTTP_404_NOT_FOUND)


@api_view(["POST", "PUT"])
@login_required_no_redirect
def store_pair(request):
    try:
        pair = Pair.objects.get(key=request.data["key"],
                                owner=request.user)
        pair_s = WritablePairSerializer(pair, data=request.data)
        if request.method == "POST":
            return Response(data={"errors": "Key already exists"},
                            status=status.HTTP_400_BAD_REQUEST)
    except Pair.DoesNotExist:
        if request.method == "PUT":
            return Response(data={"errors": "Key not found"},
                            status=status.HTTP_404_NOT_FOUND)
        pair_s = WritablePairSerializer(data=request.data)

    if pair_s.is_valid():
        pair_s.save(owner=request.user)
        if request.method == "PUT":
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response(status=status.HTTP_201_CREATED)
    else:
        return Response({"errors": pair_s.errors},
                        status=status.HTTP_400_BAD_REQUEST)
