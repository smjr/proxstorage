from django.core.management.base import BaseCommand, CommandError
from django.db.utils import IntegrityError
from django.contrib.auth import get_user_model

User = get_user_model()


class Command(BaseCommand):
    help = "Creates a simple user with a given username and optional password."

    def add_arguments(self, parser):
        parser.add_argument("username", nargs=1, type=str)

        # Optional
        parser.add_argument(
            "--password",
            dest="password",
            type=str,
            help="Will use this password rather than 'nothing'."
        )

    def handle(self, *args, **options):
        username = options["username"][0]

        try:
            user = User.objects.create(username=username)
        except IntegrityError:
            raise CommandError("User {} already exists!".format(username))

        password = options.get("password") or "nothing"
        user.set_password(password)
        user.save()

        self.stdout.write(self.style.SUCCESS(
            "Created user {} with password {}".format(username, password)
        ))
