# Proxama Key-Value Storage Project

Runs in Python 2.7 and Python 3.5.

The tests in *storer/tests/test\_decorators.py* won't run on Python 2.7 due to
the Mock module not being present. The decorator is, however, tested indirectly
in *storer/tests/test\_views.py*.

## Installation Instructions

Create a virtual environment and activate it:

    $ virtualenv venv
    $ source ./venv/bin/activate

Clone this repository and enter it:

    $ git clone https://bitbucket.org/smjr/proxstorage.git
    $ cd proxstorage

Install requirements:

    $ pip install -r requirements.txt

## Running

To run tests, run:

    $ ./manage.py migrate
    $ ./manage.py tests

To run a test server:

    $ ./manage.py runserver

To create a user:

    $ ./manage.py createuser <username> [--password <password>]

## API

HTTP Basic Authorisation is required for each endpoint.

* `http://<server_url>/api/get/`  
  Returns all key value pairs as a list of JSON objects with `key` and `value`
  attributes.

* `http://<server_url>/api/get/<key_id>/`  
  Returns a single pair as a JSON object with `key` and `value` attributes. Will
  return a 404 if the key cannot be found.

* `http://<server_url>/api/store`  
  * **POST**  
    With a POST request supplying `key` (1-20 characters long) and a `value`
    (0-100 characters long) parameters in JSON, creates a key-value pair. If the
    parameters are not properly supplied, a 400 response will be returned with a
    JSON object. The errors for each field can be found in the `errors`
    attribute.
  * **PUT**
    With a PUT request supplying an existing `key` (1-20 characters long) and a
    `value` (0-100 characters long) parameters in JSON, creates a key-value
    pair. If the parameters are not properly supplied, a 400 response will be
    returned with a JSON object. The errors for each field can be found in the
    `errors` attribute.
